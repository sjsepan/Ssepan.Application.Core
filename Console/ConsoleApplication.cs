﻿// #define debug

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using System.Reflection;

namespace Ssepan.Application.Core
{
    public static class ConsoleApplication
    {
        #region Declarations
        public const string DefaultCommandLineSwitchIndicators = "/-";
        public const string DefaultCommandLineSwitchValueSeparator = ":";
		#endregion Declarations

		#region Constructors
		static ConsoleApplication()
        {
            //add default Help functionality.
            AddCommandLineSwitch
            (
                [
                    new CommandLineSwitch("h", "Help; (this feature)", false, Help),
                    new CommandLineSwitch("H", "Help; (this feature)", false, Help),
                    new CommandLineSwitch("?", "Help; (this feature)", false, Help)
                ]
            );
        }
		#endregion Constructors

		#region Properties
		public static bool HelpInvoked { get; set; }

		/// <summary>
		/// Characters that indicated a command-line switch.
		/// </summary>
		public static string CommandLineSwitchIndicators { get; set; } = DefaultCommandLineSwitchIndicators;
		/// <summary>
		/// Characters that separate a command-line switch from a value.
		/// </summary>
		public static string CommandLineSwitchValueSeparator { get; set; } = DefaultCommandLineSwitchValueSeparator;

		/// <summary>
		/// Formal list of switches allowed.
		/// </summary>
		public static List<CommandLineSwitch> CommandLineSwitches { get; set; } = [];

		/// <summary>
		/// Actual list of switches (and values) passed.
		/// </summary>
		public static Dictionary<string, string> Arguments { get; set; }

        public static Action<string> DefaultOutputDelegate { get; set; } = writeLineWrapperOutputDelegate;
        public static Action<string> DefaultErrorOutputDelegate { get; set; } = writeErrorLineWrapperOutputDelegate;
		#endregion Properties

		#region Public Methods
		/// <summary>
		/// Load switch definitions and delegates,
		///  parse passed arguments into switches and value,
		///  and run defined actions for passed switches and values.
		/// </summary>
		/// <param name="args"></param>
		/// <param name="commandLineSwitches"></param>
		public static void DoCommandLineSwitches(string[] args, CommandLineSwitch[] commandLineSwitches)
        {
			//define supported switches
			AddCommandLineSwitch(commandLineSwitches);

            //parse switches
            if (!ParseArguments(args))
            {
                throw new ApplicationException("Unable to parse arguments.");
            }

            //run switch actions
            if (!ProcessArguments())
            {
                throw new ApplicationException("Unable to process arguments.");
            }
        }

        /// <summary>
        /// Add one or more CommandLineSwitches to list
        /// </summary>
        /// <param name="commandLineSwitches"></param>
        /// <returns></returns>
        public static bool AddCommandLineSwitch(CommandLineSwitch[] commandLineSwitches)
        {
            bool returnValue = default;
            try
            {
                foreach (CommandLineSwitch commandLineSwitch in commandLineSwitches)
                {
                    AddCommandLineSwitch(commandLineSwitch);
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Add a CommandLineSwitch to list
        /// </summary>
        /// <param name="commandLineSwitch"></param>
        /// <returns></returns>
        public static bool AddCommandLineSwitch(CommandLineSwitch commandLineSwitch)
        {
            bool returnValue = default;
            try
            {
                //check for duplicates first
                if (CommandLineSwitches.Contains(CommandLineSwitches.Find(cls => cls.SwitchCharacter == commandLineSwitch.SwitchCharacter)))
                {
                    RemoveCommandLineSwitch(commandLineSwitch);
                }
                CommandLineSwitches.Add(commandLineSwitch);

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Remove one or more CommandLineSwitch from list
        /// </summary>
        /// <param name="commandLineSwitches"></param>
        /// <returns></returns>
        public static bool RemoveCommandLineSwitch(CommandLineSwitch[] commandLineSwitches)
        {
            bool returnValue = default;
            try
            {
                foreach (CommandLineSwitch commandLineSwitch in commandLineSwitches)
                {
                    RemoveCommandLineSwitch(commandLineSwitch);
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Remove a CommandLineSwitch from list
        /// </summary>
        /// <param name="commandLineSwitch"></param>
        /// <returns></returns>
        public static bool RemoveCommandLineSwitch(CommandLineSwitch commandLineSwitch)
        {
            bool returnValue = default;
            try
            {
                returnValue = CommandLineSwitches.Remove(CommandLineSwitches.Find(cls => cls.SwitchCharacter == commandLineSwitch.SwitchCharacter));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Parse arguments into switches and values.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool ParseArguments
        (
            string[] args
        )
        {
            bool returnValue = default;
            string argument = string.Empty;
            string argumentWithoutSwitchIndicator = string.Empty;
            string[] argumentFieldsArray = default;
            string key = string.Empty;
            string value = string.Empty;
            CommandLineSwitch commandLineSwitch = default;
            Arguments = [];

            try
            {
                #if debug
                Console.WriteLine(args.Length.ToString());
                #endif

                for (int i = args.GetLowerBound(0); i <= args.GetUpperBound(0); i++)
                {
                    argument = args[i].Trim();

                    //check for missing argument
                    if (argument?.Length == 0)
                    {
                        throw new ArgumentException("Unexpected empty argument.");
                    }

                    //check for switch indicator
                    if (!CommandLineSwitchIndicators.Contains(argument[0]))
                    {
                        throw new ArgumentException(string.Format("Unexpected argument switch indicator: '{0}'", argument[0]));
                    }

                    //remove switch indicator
                    argumentWithoutSwitchIndicator = argument[1..];

                    #if debug
                    Console.WriteLine(argumentWithoutSwitchIndicator);
                    #endif

                    if (argumentWithoutSwitchIndicator.Contains(CommandLineSwitchValueSeparator))
                    {
                        //a compound switch
                        argumentFieldsArray = argumentWithoutSwitchIndicator.Split(CommandLineSwitchValueSeparator.ToCharArray());

                        if (argumentFieldsArray.Length < 1 || argumentFieldsArray.Length > 2)
                        {
                            throw new ArgumentException(string.Format("Unexpected argument format: '{0}'", argumentWithoutSwitchIndicator));
                        }

                        key = argumentFieldsArray[0].Trim();
                        if (key?.Length == 0)
                        {
                            throw new ArgumentException(string.Format("Empty argument key. Argument: '{0}'", argument));
                        }
                        if (key.Length > 1)
                        {
                            throw new ArgumentException(string.Format("Unexpected argument key format; key should be single character. Argument: '{0}'", argument));
                        }

                        value = argumentFieldsArray[1].Trim();
                        if (value?.Length == 0)
                        {
                            throw new ArgumentException(string.Format("Empty argument value. Argument: '{0}'", argument));
                        }
                    }
                    else
                    {
                        //simple switch
                        if (argumentWithoutSwitchIndicator.Length != 1)
                        {
                            //length greater than 1; not a valid simple switch
                            throw new ArgumentException(string.Format("Unexpected argument format. Separator not found: '{0}'", argumentWithoutSwitchIndicator));
                        }
                        else
                        {
                            //length is 1; valid simple switch
                            key = argumentWithoutSwitchIndicator;
                            value = string.Empty;
                        }
                    }

                    #if debug
                    Console.WriteLine(key);
                    Console.WriteLine(value);
                    #endif

                    //check for switch 
                    commandLineSwitch = CommandLineSwitches.Find(cls => cls.SwitchCharacter == key);
                    if (commandLineSwitch == null)
                    {
                        throw new ArgumentException(string.Format("Unexpected argument switch: '{0}'", key[0]));
                    }

                    //validate against UsesValue connectionString
                    if (commandLineSwitch.UsesValue)
                    {
                        if (value?.Length == 0 || value == null)
                        {
                            throw new ArgumentException(string.Format("Switch '{0}' is missing a value.", key[0]));
                        }
                    }
					else if (value != string.Empty && value != null)
					{
						throw new ArgumentException(string.Format("Switch '{0}' does not take a value.", key[0]));
					}

					//add item to dictionary
					Arguments.Add(key, value);

                    //clean up this iteration
                    argument = string.Empty;
                    argumentWithoutSwitchIndicator = string.Empty;
                    argumentFieldsArray = default;
                    key = string.Empty;
                    value = string.Empty;
                }

                #if debug
                foreach (KeyValuePair<string, string> kvp in Arguments)
                {
                    Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                }
                #endif

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }

        /// <summary>
        /// For each argument actually passed, run defined action.
        /// </summary>
        /// <returns>bool</returns>
        public static bool ProcessArguments()
        {
            bool returnValue = default;
            CommandLineSwitch commandLineSwitch = default;

            try
            {
                //process arguments passed to app
                foreach (KeyValuePair<string, string> argument in Arguments)
                {
                    commandLineSwitch = CommandLineSwitches.Find(cls => cls.SwitchCharacter == argument.Key);
                    if (commandLineSwitch != null)
                    {
                        if (commandLineSwitch.ActionDelegate != null)
                        {
                            commandLineSwitch.ActionDelegate(argument.Value, DefaultOutputDelegate/*messageBoxWrapperOutputDelegate*/);
                        }
                        else
                        {
                            throw new ApplicationException(string.Format("Switch has no action defined: {0}", argument.Key));
                        }
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Switch not recognized: {0}", argument.Key));
                    }
                }

                #if debug
                foreach (CommandLineSwitch cls in CommandLineSwitches)
                {
                    cls.ActionDelegate?.Invoke("test", ConsoleApplication.DefaultOutputDelegate/*messageBoxWrapperOutputDelegate*/);
                }
                #endif

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
            return returnValue;
        }

        #region CommandLineSwitch Action Delegates
        /// <summary>
        /// Instance of an action conforming to delegate Action<TStruct>, where TStruct is string.
        /// Built-in Help function for switches. Can be overridden.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="outputDelegate"></param>
        public static void Help
        (
            string value,
            Action<string> outputDelegate
        )
        {
			try
			{
                HelpInvoked = true;

				StringBuilder stringBuilder = new();

				outputDelegate ??= DefaultOutputDelegate;

				stringBuilder.AppendFormat("\r\nHELP\r\n").AppendLine();
                foreach (CommandLineSwitch commandLineSwitch in CommandLineSwitches)
                {
					stringBuilder.AppendFormat("\t{0}:\t{1}", commandLineSwitch.SwitchCharacter, commandLineSwitch.Description).AppendLine();
                }
                outputDelegate(stringBuilder.ToString());
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
        }
        #endregion CommandLineSwitch Action Delegates

        #region Output Delegates
        //default

        //wrapper for WriteLine
        public static readonly Action<string> writeLineWrapperOutputDelegate = Console.WriteLine;
        public static readonly Action<string> writeErrorLineWrapperOutputDelegate = Console.Error.WriteLine;
        #endregion Output Delegates

        #endregion Public Methods

    }
}
