﻿using System;

namespace Ssepan.Application.Core
{
    public class CommandLineSwitch
    {
        #region Constructors
        //public CommandLineSwitch()
        //{ 
        //}

        public CommandLineSwitch
        (
            string switchCharacter,
            string description,
            bool usesValue,
            Action<string, Action<string>> actionDelegate
        )
        {
            SwitchCharacter = switchCharacter;
            Description = description;
            UsesValue = usesValue;
            ActionDelegate = actionDelegate;
        }

		#endregion Constructors
		#region Properties
		public string SwitchCharacter { get; set; }
		public string Description { get; set; }
		public bool UsesValue { get; set; }
		public Action<string, Action<string>> ActionDelegate { get; set; }
		#endregion Properties

	}
}
