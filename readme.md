# readme.md - README for Ssepan.Application.Core

## About

Common library of application functions for C# .Net Core applications; requires ssepan.io.core, ssepan.utility.core

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### History

6.0:
~update to net8.0
~refactor to newer C# features
~refactor to existing language types
~perform format linting

5.4:
~Added properties Message2, Message3 to MessageDialogInfo to support certain feature of Qml[.Net] Message Dialog fields.
~Added property Busy to DialogInfoBase to support certain features of Qml[.Net] Dialogs.

5.3:
~Add About properties to AssemblyInfoBase for Eto.Forms: WebsiteLabel, Designers, Developers, Documenters, License

5.2:
~Add Website property to AssemblyInfoBase

5.1:
~Add SelectFolders property to Dialogs.FileDialogInfo

5.0:
~Add readme
~Refactor Ssepan.Application.Core.GtkSharp into several libraries. Created new library Ssepan.Application.Core to hold all classes not specific to a particular UI (GtkSharp GUI, Console, etc), and moved those classes there.

Steve Sepan
<sjsepan@yahoo.com>
2/28/2024
