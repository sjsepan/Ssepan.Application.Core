﻿using System;
using System.Reflection;

namespace Ssepan.Application.Core
{
    #region " Helper class to get information for the About form. "
    // This class uses the System.Reflection.Assembly class to
    // access assembly meta-data
    // This class is ! a normal feature of AssemblyInfo.cs
    public class AssemblyInfoBase<TWindow>
    {
        // Used by Helper Functions to access information from Assembly Attributes
        protected Type myType;

        public AssemblyInfoBase()
        {
            myType = typeof(TWindow);
        }

        public string AsmName
        {
            get
            {
                return myType.Assembly.GetName().Name;
            }
        }

        public string AsmFQName
        {
            get
            {
                return myType.Assembly.GetName().FullName;
            }
        }

        public string CodeBase
        {
            get
            {
                return myType.Assembly.Location;//myType.Assembly.CodeBase;
            }
        }

        public string Copyright
        {
            get
            {
                Type at = typeof(AssemblyCopyrightAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyCopyrightAttribute ct = (AssemblyCopyrightAttribute)r[0];
                return ct.Copyright;
            }
        }

        public string Company
        {
            get
            {
                Type at = typeof(AssemblyCompanyAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyCompanyAttribute ct = (AssemblyCompanyAttribute)r[0];
                return ct.Company;
            }
        }

        public string Description
        {
            get
            {
                Type at = typeof(AssemblyDescriptionAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyDescriptionAttribute da = (AssemblyDescriptionAttribute)r[0];
                return da.Description;
            }
        }

        public string Product
        {
            get
            {
                Type at = typeof(AssemblyProductAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyProductAttribute pt = (AssemblyProductAttribute)r[0];
                return pt.Product;
            }
        }

        public string Title
        {
            get
            {
                Type at = typeof(AssemblyTitleAttribute);
                object[] r = myType.Assembly.GetCustomAttributes(at, false);
                AssemblyTitleAttribute ta = (AssemblyTitleAttribute)r[0];
                return ta.Title;
            }
        }

        public string Version
        {
            get
            {
                return myType.Assembly.GetName().Version.ToString();
            }
        }
		public string Website { get; set; }
		public string WebsiteLabel { get; set; }
		public string[] Designers { get; set; }
		public string[] Developers { get; set; }
		public string[] Documenters { get; set; }
		public string License { get; set; }
	}
    #endregion

}