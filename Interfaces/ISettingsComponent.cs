using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Ssepan.Application.Core
{
    /// <summary>
    /// Interface for SettingsComponent, which are implemented by either SettingsBase or any complex property used in a descendent of SettingsBase.
    /// </summary>
    public interface ISettingsComponent :
        IDisposable,
        INotifyPropertyChanged,
        IEquatable<ISettingsComponent>
    {
        #region Properties
        [XmlIgnore]
        bool Dirty
        {
            get;
        }
        #endregion Properties

        #region non-static methods
        /// <summary>
        /// Copies property values from source working fields to destination working fields, then optionally syncs destination.
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="sync"></param>
        void CopyTo(ISettingsComponent destination, bool sync);

        /// <summary>
        /// Syncs property values by copying from working fields to reference fields.
        /// </summary>
        void Sync();
        #endregion non-static methods

    }
}
