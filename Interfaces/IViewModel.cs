﻿using System;
using System.ComponentModel;

namespace Ssepan.Application.Core
{
    /// <summary>
    /// IViewModel
    /// </summary>
    /// <typeparam name="TIcon"></typeparam>
    public interface IViewModel<TIcon> :
        INotifyPropertyChanged
        where TIcon : class
    {
        #region INotifyPropertyChanged
        //event PropertyChangedEventHandler PropertyChanged;
        #endregion INotifyPropertyChanged

        #region Properties
        string StatusMessage { get; set; }
        string ErrorMessage { get; set; }
        string ErrorMessageToolTipText { get; set; }
        int ProgressBarValue { get; set; }
        int ProgressBarMaximum { get; set; }
        int ProgressBarMinimum { get; set; }
        int ProgressBarStep { get; set; }
        bool ProgressBarIsMarquee { get; set; }
        bool ProgressBarIsVisible { get; set; }
        bool ActionIconIsVisible { get; set; }
        TIcon ActionIconImage { get; set; }
        bool DirtyIconIsVisible { get; set; }
        TIcon DirtyIconImage { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Use when Marquee-style progress bar is not sufficient, and percentages must be indicated.
        /// WPF.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="errorMessage"></param>
        /// <param name="objImage">System.Windows.Controls.Image or System.Drawing.Image</param>
        /// <param name="isMarqueeProgressBarStyle"></param>
        /// <param name="progressBarValue"></param>
        /// <param name="doEventsWrapperDelegate"></param>
        void StartProgressBar
        (
            string statusMessage,
            string errorMessage,
            TIcon objImage,
            bool isMarqueeProgressBarStyle,
            int progressBarValue,
            Action doEventsWrapperDelegate = null
        );

        /// <summary>
        /// Update percentage changes.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="progressBarValue"></param>
        /// <param name="doEventsWrapperDelegate"></param>
        void UpdateProgressBar
        (
            string statusMessage,
            int progressBarValue,
            Action doEventsWrapperDelegate = null
        );

        /// <summary>
        /// Update message(s) only, without changing progress bar.
        /// Null parameter will leave a message unchanged;
        /// string.Empty will clear it.
        /// Optional doEvents flag will determine if
        /// messages are processed before continuing.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="errorMessage"></param>
        /// <param name="customMessage"></param>
        /// <param name="doEventsWrapperDelegate"></param>
        void UpdateStatusBarMessages
        (
            string statusMessage,
            string errorMessage,
            string customMessage = null,
            Action doEventsWrapperDelegate = null
        );

        /// <summary>
        /// Stop progress bar and display messages.
        /// DoEvents will ensure messages are processed before continuing.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="errorMessage"></param>
        /// <param name="doEventsWrapperDelegate"></param>
        void StopProgressBar
        (
            string statusMessage,
            string errorMessage = null,
            Action doEventsWrapperDelegate = null
        );
        #endregion Methods

    }
}
