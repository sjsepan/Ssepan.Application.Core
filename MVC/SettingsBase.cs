using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Ssepan.Utility.Core;

namespace Ssepan.Application.Core
{
	/// <summary>
	/// Base for Settings which are persisted.
	/// </summary>
    [DataContract(IsReference=true)]
    [Serializable]
    public abstract class SettingsBase :
        ISettings
    {
        #region Declarations
        protected bool disposed;

        //private const string FILE_TYPE_EXTENSION_XML = "xml"; 
        private const string FILE_TYPE_EXTENSION_JSON = "json";
        private const string FILE_TYPE_NAME = "settingsfile";
        private const string FILE_TYPE_DESCRIPTION = "Settings Files";

        public enum SerializationFormat
        {
            Xml,
            DataContract,
            Json
        }
        #endregion Declarations

        #region Constructors
        protected SettingsBase()
        {
        }
        #endregion Constructors

        #region IDisposable
        ~SettingsBase()
        {
            Dispose(false);
        }

        public virtual void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            // tell the GC that the Finalize process no longer needs
            // to be run for this object.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                //Resources not disposed
                if (disposeManagedResources)
                {
                    // dispose managed resources
                    //if (_xxx != null)
                    //{
                    //    _xxx = null;
                    //}
                }
                // dispose unmanaged resources
                disposed = true;
            }
        }
        #endregion IDisposable

        #region INotifyPropertyChanged 
        //If property of ISettings object changes, fire OnPropertyChanged, which notifies any subscribed observers by calling PropertyChanged.
        //Called by all 'set' statements in ISettings object properties.
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

				//any property that can fire OnPropertyChanged can affect the value Dirty, which should be recalculated on demand.
				if (propertyName != "Dirty")
                {
                    OnPropertyChanged(nameof(Dirty));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
        }
        #endregion INotifyPropertyChanged 

        #region IEquatable<ISettingsComponent>
        /// <summary>
        /// Compare property values of two specified Settings objects.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public virtual bool Equals(/*ISettings*/ISettingsComponent other)
        {
			bool returnValue;
			try
			{
				SettingsBase otherSettings = other as SettingsBase;
				if (this == otherSettings)
				{
					returnValue = true;
				}
				else
				{
					// if (false/*this.Xxx != otherSettings.Xxx*/)
					// {
					//     returnValue = false;
					// }
					// else
					// {
					returnValue = true;
					// }
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				throw;
			}

			return returnValue;
        }

		#endregion IEquatable<ISettingsComponent>
		#region Properties
		[XmlIgnore]
		public static string FileTypeExtension { get; set; } = FILE_TYPE_EXTENSION_JSON;
		[XmlIgnore]
		public static string FileTypeName { get; set; } = FILE_TYPE_NAME;
		[XmlIgnore]
		public static string FileTypeDescription { get; set; } = FILE_TYPE_DESCRIPTION;
		[XmlIgnore]
		public static SerializationFormat SerializeAs { get; set; }

		[XmlIgnore]
        public virtual bool Dirty
        {
            get
            {
				bool returnValue;
				try
                {
                    // if (false/*_Xxx != __Xxx*/)
                    // {
                    //     returnValue = true;
                    // }
                    // else
                    // {
                        returnValue = false;
                    // }
                }
                catch (Exception ex)
                {
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                    throw;
                }

                return returnValue;
            }
        }
        #endregion Properties

        #region Methods
        #region Static Methods
        /// <summary>
        /// converts string value into corresponding SerializationFormat enum
        /// </summary>
        /// <param name="value">source string</param>
        public static SerializationFormat ToSerializationFormat
        (
            string value
        )
        {
			return value.ToLower() switch
			{
				"xml" => SerializationFormat.Xml,
				"datacontract" => SerializationFormat.DataContract,
				"json" => SerializationFormat.Json,
				_ => SerializationFormat.Xml,
			};
		}
        #endregion Static Methods

        /// <summary>
        /// Copies property values from source working fields to destination working fields, then optionally syncs destination.
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="sync"></param>
        public virtual void CopyTo(/*ISettings*/ISettingsComponent destination, bool sync)
        {
			try
			{
				ISettings destinationSettings = destination as ISettings;

				//destinationSettings.Xxx = this.Xxx;

				if (sync)
                {
                    destinationSettings.Sync();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Syncs property values by copying from working fields to reference fields.
        /// </summary>
        public virtual void Sync()
        {
            try
            {
                //__Xxx = _Xxx;

                //Note:where we have cloned collections; the collection comparison will never find the original items in the cloned collection if it is looking at identity vs content--SJS
                //if (Dirty)
                //{
                //    throw new ApplicationException("Sync failed.");
                //}
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Update child components (used as properties) to use the passed handler.
        /// </summary>
        public virtual void UpdateHandlers(){}
        #endregion Methods

    }
}
