﻿using System;
using System.ComponentModel;
using System.Reflection;
using Ssepan.Utility.Core;

namespace Ssepan.Application.Core
{
    /// <summary>
    /// Manager for the run-time model.
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public class ModelController<TModel>
        where TModel :
            class,
            IModel,
            new()
    {
        #region Declarations
        protected static bool _ValueChanging {get; set;} //used by controller methods that could trigger notifications and refresh while processing
        protected static bool _NoUiOnThisThread {get; set;} //use with controller operations run from non-UI thread
        #endregion Declarations

        #region Properties
        private static TModel _Model;
        public static TModel Model
        {
            get { return _Model; }
            set
            {
				if (DefaultHandler != null && Model != null)
				{
					Model.PropertyChanged -= DefaultHandler;
				}

				_Model = value;

				if (DefaultHandler != null && Model != null)
				{
					Model.PropertyChanged += DefaultHandler;
				}
			}
        }

        private static PropertyChangedEventHandler _DefaultHandler;
        /// <summary>
        /// Handler to assigned to Settings on New, Open.
        /// </summary>
        public static PropertyChangedEventHandler DefaultHandler
        {
            get { return _DefaultHandler; }
            set
            {
				if (DefaultHandler != null && Model != null)
				{
					Model.PropertyChanged -= DefaultHandler;
				}

				_DefaultHandler = value;

				if (DefaultHandler != null && Model != null)
				{
					Model.PropertyChanged += DefaultHandler;
				}
			}
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// New settings
        /// </summary>
        /// <returns></returns>
        public static bool New()
        {
            bool returnValue = default;
            try
            {
                //create new object
                Model = new TModel();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
