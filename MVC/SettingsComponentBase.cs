using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Ssepan.Utility.Core;

namespace Ssepan.Application.Core
{
    /// <summary>
    /// Base for component of Settings which are persisted.
    /// </summary>
    [DataContract(IsReference = true)]
    [Serializable]
    public abstract class SettingsComponentBase :
        ISettingsComponent
    {
        #region Declarations
        protected bool disposed;
        #endregion Declarations

        #region Constructors
        protected SettingsComponentBase()
        {
        }
        #endregion Constructors

        #region IDisposable
        ~SettingsComponentBase()
        {
            Dispose(false);
        }

        public virtual void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            // tell the GC that the Finalize process no longer needs
            // to be run for this object.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                //Resources not disposed
                if (disposeManagedResources)
                {
                    // dispose managed resources
                    //if (_xxx != null)
                    //{
                    //    _xxx = null;
                    //}
                }
                // dispose unmanaged resources
                disposed = true;
            }
        }
        #endregion IDisposable

        #region INotifyPropertyChanged 
        //If property of ISettingsComponent object changes, fire OnPropertyChanged, which notifies any subscribed observers by calling PropertyChanged.
        //Called by all 'set' statements in ISettingsComponent object properties.
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
				this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

				//any property that can fire OnPropertyChanged can affect the value Dirty, which should be recalculated on demand.
				if (propertyName != "Dirty")
                {
                    OnPropertyChanged(nameof(Dirty));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
        }
        #endregion INotifyPropertyChanged 

        #region IEquatable<ISettingsComponent>
        /// <summary>
        /// Compare property values of two specified Settings objects.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public virtual bool Equals(ISettingsComponent other)
        {
			bool returnValue;
			try
			{
				SettingsComponentBase otherSettings = other as SettingsComponentBase;
				if (this == otherSettings)
				{
					returnValue = true;
				}
				else
				{
					// if (false/*this.Xxx != otherSettings.Xxx*/)
					// {
					//     returnValue = false;
					// }
					// else
					// {
					returnValue = true;
					// }
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				throw;
			}

			return returnValue;
        }
        #endregion IEquatable<ISettingsComponent>

        #region Properties
        [XmlIgnore]
        public virtual bool Dirty
        {
            get
            {
				bool returnValue;
				try
                {
                    // if (false/*_Xxx != __Xxx*/)
                    // {
                    //     returnValue = true;
                    // }
                    // else
                    // {
                        returnValue = false;
                    // }
                }
                catch (Exception ex)
                {
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                    throw;
                }

                return returnValue;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Copies property values from source working fields to destination working fields, then optionally syncs destination.
        /// </summary>
        /// <param name="destination"></param>
        /// <param name="sync"></param>
        public virtual void CopyTo(ISettingsComponent destination, bool sync)
        {
			try
			{
				ISettingsComponent destinationSettings = destination;

				//destinationSettings.Xxx = this.Xxx;

				if (sync)
                {
                    destinationSettings.Sync();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Syncs property values by copying from working fields to reference fields.
        /// </summary>
        public virtual void Sync()
        {
            try
            {
                //__Xxx = _Xxx;

                if (Dirty)
                {
                    throw new ApplicationException("Sync failed.");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion Methods

    }
}
