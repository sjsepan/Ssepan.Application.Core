using System;
using System.ComponentModel;
using System.Reflection;
using Ssepan.Utility.Core;

namespace Ssepan.Application.Core
{
    /// <summary>
    /// Base type for components of run-time Model.
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public abstract class ModelComponentBase :
        IModelComponent
    {
        #region Declarations
        protected bool disposed;
        #endregion Declarations

        #region Constructors
        protected ModelComponentBase()
        {
        }
        #endregion Constructors

        #region IDisposable
        ~ModelComponentBase()
        {
            Dispose(false);
        }

        public virtual void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            // tell the GC that the Finalize process no longer needs
            // to be run for this object.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                //Resources not disposed
                if (disposeManagedResources)
                {
                    // dispose managed resources
                    //if (_xxx != null)
                    //{
                    //    _xxx = null;
                    //}
                }
                // dispose unmanaged resources
                disposed = true;
            }
        }
        #endregion IDisposable

        #region INotifyPropertyChanged 
        //If property of IModelComponent object changes, fire OnPropertyChanged, which notifies any subscribed observers by calling PropertyChanged.
        //Called by all 'set' statements in IModelComponent object properties.
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
        }
        #endregion INotifyPropertyChanged 

        #region IEquatable<IModelComponent>
        /// <summary>
        /// Compare property values of two specified Model objects.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public virtual bool Equals(IModelComponent other)
        {
			bool returnValue;
			try
			{
				ModelComponentBase otherModel = other as ModelComponentBase;
				if (this == otherModel)
				{
					returnValue = true;
				}
				else
				{
					// if (false/*this.Xxx != otherModel.Xxx*/)
					// {
					//     returnValue = false;
					// }
					// else
					// {
					returnValue = true;
					// }
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				throw;
			}

			return returnValue;
        }
        #endregion IEquatable<IModelComponent>

        #region Properties
        private bool _IsChanged;
        /// <summary>
        /// Used when binding is not available.
        /// Value doesn't matter; setting value from controller Refresh
        /// fires PropertyChanged event that tells viewer(s) to apply changes
        /// </summary>
        public bool IsChanged
        {
            get { return _IsChanged; }
            set
            {
                _IsChanged = value;
                OnPropertyChanged(nameof(IsChanged));
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Support clients that do not handle databinding, but which can subscribe to PropertyChanged.
        /// Additionally, while clients can handle PropertyChanged on individual properties,
        ///  this is a general notification that the client may desire to update bindings.
        /// </summary>
        public virtual void Refresh()
        {
            IsChanged = true;//Value doesn't matter; fire a changed event;
        }

        public virtual void Refresh(string propertyName)
        {
            OnPropertyChanged(propertyName);
        }
        #endregion Methods

    }
}
