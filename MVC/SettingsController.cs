using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
using Ssepan.Utility.Core;
using Ssepan.Io.Core;

namespace Ssepan.Application.Core
{
    /// <summary>
    /// Manager for the persisted Settings.
    /// </summary>
    /// <typeparam name="TSettings"></typeparam>
    public static class SettingsController<TSettings> //static
        where TSettings :
            class,
            ISettings,
            new()
    {
        #region Declarations
        public const string FILE_NEW = "(new)";
        #endregion Declarations

        #region Properties
        private static TSettings _Settings;
        public static TSettings Settings//TODO:notify here as well?
        {
            get { return _Settings; }
            set
            {
				if (DefaultHandler != null && _Settings != null)
				{
					_Settings.PropertyChanged -= DefaultHandler;
				}

				_Settings = value;

				if (DefaultHandler != null && _Settings != null)
				{
					_Settings.PropertyChanged += DefaultHandler;
				}
			}
        }
		/// <summary>
		/// Previous value of Pathname
		/// Set when filename changes.  Not synchronized here, but client apps may override and synchronize if it suits them.
		/// </summary>
		public static string OldPathname { get; set; }
		/// <summary>
		/// Previous value of Filename.
		/// Set when filename changes.  Not synchronized here, but client apps may override and synchronize if it suits them.
		/// </summary>
		public static string OldFilename { get; set; }

		private static string _Filename = FILE_NEW;
        /// <summary>
        /// Filename component of FilePath
        /// </summary>
        public static string Filename
        {
            get { return _Filename; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    //just clear property
                    OldFilename = _Filename;//remember previous name
                    _Filename = string.Empty;
                }
                else if (Path.GetDirectoryName(value) != string.Empty)
                {
                    //send to be split first
                    FilePath = value;
                }
                else
                {
                    //just set property to value
                    OldFilename = _Filename;//remember previous name
                    _Filename = value;
                }
            }
        }

        private static string _Pathname = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).WithTrailingSeparator();
        /// <summary>
        /// Path component of FilePath
        /// </summary>
        public static string Pathname
        {
            get { return _Pathname; }
            set
            {
                const string PREFIX = "file:";//Note: prefix may work in Nemo, but is causing problems here
                string newValue = value.Replace(PREFIX, "");//Remove(0, PREFIX.Length);//only if present

                // Console.WriteLine("Pathname:value="+value);
                // Console.WriteLine("Pathname:newValue="+newValue);
                if (string.IsNullOrEmpty(newValue))
                {
                    //just clear property
                    OldPathname = _Pathname;
                    _Pathname = newValue;
                }
                else if (Path.GetFileName(newValue) != string.Empty)
                {
                    //send to be split first
                    FilePath = newValue;
                }
                else
                {
                    //just set property to newValue
                    OldPathname = _Pathname;
                    _Pathname = newValue;
                }
            }
        }

        /// <summary>
        /// Combined value of Pathname and Filename
        /// </summary>
        public static string FilePath
		{
			get =>
				//retrieve and combine
				// Console.WriteLine("FilePath:Pathname="+Pathname);
				// Console.WriteLine("FilePath:Filename="+Filename);
				Path.Combine(Pathname, Filename);
			set
			{
				//split and store
				Filename = Path.GetFileName(value);
				if (!Path.GetDirectoryName(value).EndsWith(Path.DirectorySeparatorChar.ToString()))
				{
					//add separator or Pathname setter will think path still contains filename
					Pathname = Path.GetDirectoryName(value).WithTrailingSeparator();
				}
			}
		}

		private static PropertyChangedEventHandler _DefaultHandler;
        /// <summary>
        /// Handler to assigned to Settings; triggered on New, Open.
        /// </summary>
        public static PropertyChangedEventHandler DefaultHandler
        {
            get { return _DefaultHandler; }
            set
            {
				if (DefaultHandler != null && Settings != null)
				{
					Settings.PropertyChanged -= DefaultHandler;
				}

				_DefaultHandler = value;

				if (DefaultHandler != null && Settings != null)
				{
					Settings.PropertyChanged += DefaultHandler;
				}
			}
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// New settings
        /// </summary>
        /// <returns>bool</returns>
        public static bool New()
        {
            bool returnValue = default;
            try
            {//DEBUG:why is settings controller calling new before defaulthandler is set?
                //create new object
                Settings = new TSettings();
                Settings.Sync();
                Filename = FILE_NEW;

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }

        /// <summary>
        /// Open settings.
        /// </summary>
        /// <returns></returns>
        public static bool Open()
        {
            bool returnValue = default;

            try
            {
				// Console.WriteLine("Open:SettingsBase.SerializeAs="+SettingsBase.SerializeAs);
				// Console.WriteLine("Open:FilePath="+FilePath);
				//read from file
				Settings = SettingsBase.SerializeAs switch
				{
					SettingsBase.SerializationFormat.Json => LoadJson(FilePath),
					SettingsBase.SerializationFormat.DataContract => LoadDataContract(FilePath),
					//case SettingsBase.SerializationFormat.Xml:
					_ => LoadXml(FilePath),
				};
				returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Save settings.
        /// </summary>
        /// <returns>bool</returns>
        public static bool Save()
        {
            bool returnValue = default;

            try
            {
                //write to file
                switch (SettingsBase.SerializeAs)
                //switch (Settings.SerializeAs)
                {
					case SettingsBase.SerializationFormat.Json:
						PersistJson(Settings, FilePath);

						break;

					case SettingsBase.SerializationFormat.DataContract:
						PersistDataContract(Settings, FilePath);

						break;

					//case SettingsBase.SerializationFormat.Xml:
					default:
						PersistXml(Settings, FilePath);

						break;
				}

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            return returnValue;
        }

        /// <summary>
        /// Loads the specified object with data from the specified file, using XML Serializer.
        /// </summary>
        /// <param name="filePath"></param>
        public static TSettings LoadXml(string filePath)
        {
			TSettings returnValue;
			try
			{
				//XML Serializer of type Settings
				Type returnValueType = typeof(TSettings);
				XmlSerializer xs = new(returnValueType);

				//Stream reader for file
				StreamReader sr = new(filePath);

				//de-serialize into Settings
				returnValue = (TSettings)xs.Deserialize(sr);
				returnValue.Sync();

				//close file
				sr.Close();
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

				throw;
			}
			return returnValue;
        }

        /// <summary>
        /// Saves the specified object's data to the specified file, using XML Serializer.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filePath"></param>
        public static void PersistXml(TSettings settings, string filePath)
        {
            try
            {
                //XML Serializer of type Settings
                XmlSerializer xs = new(settings.GetType());

                //Stream writer for file
                StreamWriter sw = new(filePath);

                //serialize out of Settings
                xs.Serialize(sw, settings);

                //close file
                sw.Close();

                settings.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Loads the specified object with data from the specified file, using DataContract Serializer.
        /// </summary>
        /// <param name="filePath"></param>
        public static TSettings LoadDataContract(string filePath)
        {
            TSettings returnValue = default;
			try
			{
				//DataContract Serializer of type Settings
				Type returnValueType = typeof(TSettings);
				DataContractSerializerSettings dataContractSerializerSettings = new()
				{
					PreserveObjectReferences = true,
					MaxItemsInObjectGraph = int.MaxValue
				};
				DataContractSerializer xs = new(returnValueType, dataContractSerializerSettings);//,null, int.MaxValue, false, true /* preserve object refs */, null

                //Stream writer for file
                using (FileStream fs = new(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    //de-serialize into Settings
                    returnValue = (TSettings)xs.ReadObject(fs);
                }

                returnValue.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Saves the specified object's data to the specified file, using DataContract Serializer.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filePath"></param>
        public static void PersistDataContract(TSettings settings, string filePath)
        {
			try
			{
				//DataContract Serializer of type Settings
				DataContractSerializerSettings dataContractSerializerSettings = new()
				{
					PreserveObjectReferences = true,
					MaxItemsInObjectGraph = int.MaxValue
				};
				DataContractSerializer xs = new(settings.GetType(), dataContractSerializerSettings);//, null, int.MaxValue, false, true /* preserve object refs */, null

                //Stream writer for file
                using (FileStream fs = new(filePath, FileMode.Create, FileAccess.Write, FileShare.Write))
                {
                    //serialize out of Settings
                    xs.WriteObject(fs, settings);
                }

                settings.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Loads the specified object with data from the specified file, using Json Serializer.
        /// </summary>
        /// <param name="filePath"></param>
        public static TSettings LoadJson(string filePath)
        {
            TSettings returnValue = default;
			try
			{
				//Json Serializer of type Settings
				Type returnValueType = typeof(TSettings);
#pragma warning disable CA1869
				JsonSerializerOptions jsonSerializerOptions = new()
				{
					ReferenceHandler = ReferenceHandler.Preserve,
					MaxDepth = int.MaxValue,
					IgnoreReadOnlyProperties = true
				};
#pragma warning restore CA1869

				//Stream writer for file
				using (FileStream fs = new(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    //de-serialize into Settings
                    returnValue = /*(TSettings)*/JsonSerializer.Deserialize<TSettings>(fs, jsonSerializerOptions);
                    // Console.WriteLine(string.Format("After opening {0} format is {1}...", filePath, SettingsBase.SerializeAs.ToString()));
                }

                returnValue.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Saves the specified object's data to the specified file, using Json Serializer.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="filePath"></param>
        public static void PersistJson(TSettings settings, string filePath)
        {
			try
			{
				//Json Serializer of type Settings
#pragma warning disable CA1869
				JsonSerializerOptions jsonSerializerOptions = new()
				{
					WriteIndented = true,
					ReferenceHandler = ReferenceHandler.Preserve,
					MaxDepth = int.MaxValue,
					IgnoreReadOnlyProperties = true
				};
#pragma warning restore CA1869

				//Stream writer for file
				using (FileStream fs = new(filePath, FileMode.Create, FileAccess.Write, FileShare.Write))
                {
                    //serialize out of Settings
                    JsonSerializer.Serialize(fs, settings, jsonSerializerOptions);
                }

                settings.Sync();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Display property grid dialog. Changes to properties are reflected immediately.
        /// </summary>
        public static void ShowProperties(Action refreshDelegate)
        {
            try
            {
                throw new NotImplementedException("PropertiesViewer not available in GtkSharp");
//                 //PropertiesViewer pv = new PropertiesViewer(SettingsBase.AsStatic, Refresh);
//                 PropertyDialog pv = new PropertyDialog(Settings, refreshDelegate);
// #if debug
//                 //pv.Show();//dialog properties grid validation does refresh
// #else
//                 pv.ShowDialog();//dialog properties grid validation does refresh
//                 pv.Dispose();
// #endif
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion Methods
    }
}
