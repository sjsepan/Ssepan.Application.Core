﻿namespace Ssepan.Application.Core
{
	public class ColorDialogInfo<TWindow, TResponseEnum, TColorDescriptor> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Constructors
        public ColorDialogInfo()
        { }

        /// <summary>
        /// ColorDialogInfo
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        /// <param name="color">TColorDescriptor</param>
        public ColorDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            TColorDescriptor color
        ) :
            base (parent, modal, title, response)
        {
            Color = color;
        }

		#endregion Constructors
		#region Properties
		public TColorDescriptor Color { get; set; }
		#endregion Properties

	}
}
