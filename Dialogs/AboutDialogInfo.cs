﻿using System;
using System.Reflection;
using Ssepan.Utility.Core;

namespace Ssepan.Application.Core
{
    public class AboutDialogInfo<TWindow, TResponseEnum, TLogo> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Declarations
        public const string CopyrightSymbol = "©";
#endregion Declarations

#region Constructors
        public AboutDialogInfo()
        { }

        /// <summary>
        /// AboutDialogInfo
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        /// <param name="programName">string</param>
        /// <param name="version">string</param>
        /// <param name="copyright">string</param>
        /// <param name="comments">string</param>
        /// <param name="website">string</param>
        /// <param name="logo">TLogo</param>
        public AboutDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string programName,
            string version,
            string copyright,
            string comments,
            string website,
            TLogo logo
        ) :
            base (parent, modal, title, response)
        {
            try
            {
                ProgramName = programName;
                Version = version;
                Copyright = copyright;
                Comments = comments;
                Website = website;
                Logo = logo;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// AboutDialogInfo
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        /// <param name="programName">string</param>
        /// <param name="version">string</param>
        /// <param name="copyright">string</param>
        /// <param name="comments">string</param>
        /// <param name="website">string</param>
        /// <param name="logo">TLogo</param>
        /// <param name="websiteLabel">string</param>
        /// <param name="designers">string[]</param>
        /// <param name="developers">string[]</param>
        /// <param name="documenters">string[]</param>
        /// <param name="license">string</param>
        public AboutDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string programName,
            string version,
            string copyright,
            string comments,
            string website,
            TLogo logo,
            string websiteLabel,
            string[] designers,
            string[] developers,
            string[] documenters,
            string license
        ) :
            this (parent, modal, title, response, programName, version, copyright, comments, website, logo)
        {
            try
            {
                WebsiteLabel = websiteLabel;
                Designers = designers;
                Developers = developers;
                Documenters = documenters;
                License = license;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

		#endregion Constructors
		#region Properties
		public string ProgramName { get; set; }
		public string Version { get; set; }
		public string Copyright { get; set; }
		public string Comments { get; set; }
		public string Website { get; set; }
		public TLogo Logo { get; set; }
		public string WebsiteLabel { get; set; }
		public string[] Designers { get; set; }
		public string[] Developers { get; set; }
		public string[] Documenters { get; set; }
		public string License { get; set; }

		#endregion Properties

	}
}
