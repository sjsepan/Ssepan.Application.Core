﻿using System;

namespace Ssepan.Application.Core
{
    public class FileDialogInfo<TWindow, TResponseEnum> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
        #region Declarations
        public const char FILTER_SEPARATOR = ',';
        public const char FILTER_ITEM_SEPARATOR = '|';
        public const string FILTER_FORMAT = "{0} (*.{1})|*.{1}";
        // public const string FILTER_DESCRIPTION = "{0} Files(s)";
        #endregion Declarations

        #region Constructors
        public FileDialogInfo()
        { }

        /// <summary>
        /// Use this constructor if you want to use the SelectFolder FileChooserAction
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        public FileDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response
        ) :
            base (parent, modal, title, response)
        {
        }

        /// <summary>
        /// Use this constructor if you want to default the location to an Environment.SpecialFolder.
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        /// <param name="newFilename">string</param>
        /// <param name="filename">string</param>
        /// <param name="extension">string</param>
        /// <param name="description">string</param>
        /// <param name="typeName">string</param>
        /// <param name="additionalFilters">string[]</param>
        /// <param name="multiselect">bool</param>
        /// <param name="initialDirectory">Environment.SpecialFolder</param>
        /// <param name="forceDialog">bool</param>
        /// <param name="forceNew">bool</param>
        /// <param name="selectFolders">bool</param>
        public FileDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string newFilename,
            string filename,
            string extension,
            string description,
            string typeName,
			string[] additionalFilters,
            bool multiselect,
            Environment.SpecialFolder initialDirectory,
            bool forceDialog,
            bool forceNew,
            bool selectFolders = false
        ) :
            base (parent, modal, title, response)
        {
            NewFilename = newFilename;
            Filename = filename;
            Extension = extension;
            Description = description;
            TypeName = typeName;
            //Join additionalFilters, create new array of those plus primary filter, then Join them again.
            Filters = string.Join(FILTER_SEPARATOR, [string.Format(FILTER_FORMAT, description, extension), string.Join(FILTER_SEPARATOR, additionalFilters)]);
            Multiselect = multiselect;
            InitialDirectory = initialDirectory;
            ForceDialog = forceDialog;
            ForceNew = forceNew;
            SelectFolders = selectFolders;
        }

        /// <summary>
        /// Use this constructor if you want to default the location to a custom location.
        /// Pass default(Environment.SpecialFolder) for InitialDirectory.
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        /// <param name="newFilename">string</param>
        /// <param name="filename">string</param>
        /// <param name="extension">string</param>
        /// <param name="description">string</param>
        /// <param name="typeName">string</param>
        /// <param name="additionalFilters">string[]</param>
        /// <param name="multiselect">bool</param>
        /// <param name="initialDirectory">Environment.SpecialFolder</param>
        /// <param name="forceDialog">bool</param>
        /// <param name="forceNew">bool</param>
        /// <param name="customInitialDirectory">string</param>
        /// <param name="selectFolders">bool</param>
        public FileDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string newFilename,
            string filename,
            string extension,
            string description,
            string typeName,
			string[] additionalFilters,
            bool multiselect,
            Environment.SpecialFolder initialDirectory,
            bool forceDialog,
            bool forceNew,
            string customInitialDirectory,
            bool selectFolders = false
        ) :
            this
            (
                parent,
                modal,
                title,
                response,
                newFilename,
                filename,
                extension,
                description,
                typeName,
                additionalFilters,
                multiselect,
                initialDirectory,
                forceDialog,
                forceNew,
                selectFolders
            )
        {
            CustomInitialDirectory = customInitialDirectory;
        }

		#endregion Constructors
		#region Properties
		public string NewFilename { get; set; }
		public string Filename { get; set; }
		public string[] Filenames { get; set; }
		public bool MustExist { get; set; }
		public bool Multiselect { get; set; }
		public string Extension { get; set; }
		public string Description { get; set; }
		public string TypeName { get; set; }
		public string Filters { get; set; }
		public Environment.SpecialFolder InitialDirectory { get; set; }
		public bool ForceDialog { get; set; }
		public bool ForceNew { get; set; }
		public string CustomInitialDirectory { get; set; }
		public bool SelectFolders { get; set; }
		#endregion Properties

	}
}
