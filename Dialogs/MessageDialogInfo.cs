﻿namespace Ssepan.Application.Core
{
	public class MessageDialogInfo<TWindow, TResponseEnum, TDialogFlags, TMessageType, TButtonsType> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Constructors
        public MessageDialogInfo()
        { }

        /// <summary>
        /// dialog info for message box
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="dialogFlags">TDialogFlags</param>
        /// <param name="messageType">TMessageType</param>
        /// <param name="buttonsType">TButtonsType</param>
        /// <param name="message">string</param>
        /// <param name="response">TResponseEnum</param>
        public MessageDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TDialogFlags dialogFlags,
            TMessageType messageType,
            TButtonsType buttonsType,
            string message,
            TResponseEnum response
        ) :
            base (parent, modal, title, response)
        {
            DialogFlags = dialogFlags;
            MessageType = messageType;
            ButtonsType = buttonsType;
            Message = message;
            Response = response;
        }

		#endregion Constructors
		#region Properties
		public TDialogFlags DialogFlags { get; set; }
		public TMessageType MessageType { get; set; }
		public TButtonsType ButtonsType { get; set; }
		public string Message { get; set; }
		public string Message2 { get; set; }
		public string Message3 { get; set; }

		#endregion Properties

	}
}
