﻿namespace Ssepan.Application.Core
{
	public class PrinterDialogInfo<TWindow, TResponseEnum, TPrinter>  :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Constructors
        public PrinterDialogInfo()
        { }

        /// <summary>
        /// PrinterDialogInfo
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        public PrinterDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response
        ) :
            base (parent, modal, title, response)
        {
        }

		#endregion Constructors//

		#region Properties
		public TPrinter Printer { get; set; }
		public string Name { get; set; }
		#endregion Properties

	}
}
