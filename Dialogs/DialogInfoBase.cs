﻿using System;

namespace Ssepan.Application.Core
{
    public class DialogInfoBase<TWindow, TResponseEnum>
    {
#region Constructors
        public DialogInfoBase()
        { }

        /// <summary>
        /// DialogInfoBase
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        /// <param name="errorMessage">string. Optional. Used when method cannot take ref params, such as async calls</param>
        public DialogInfoBase
        (
           TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            string errorMessage = null
        )
        {
            Parent = parent;
            Modal = modal;
            Title = title;
            Response = response;
            ErrorMessage = errorMessage;
        }

		#endregion Constructors
		#region Properties
		public TWindow Parent { get; set; }
		public bool Modal { get; set; }
		public string Title { get; set; }
		public TResponseEnum Response { get; set; }
		public string ErrorMessage { get; set; }
		public bool BoolResult { get; set; }
		/// <summary>
		/// Semaphore used in some cases (Qml.Net) to indicate
		/// dialog was started and has not completed yet.
		/// </summary>
		public bool Busy { get; set; }

		#endregion Properties

	}
}
