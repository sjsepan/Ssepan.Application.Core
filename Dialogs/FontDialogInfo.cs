﻿namespace Ssepan.Application.Core
{
	public class FontDialogInfo<TWindow, TResponseEnum, TFontDescriptor> :
        DialogInfoBase<TWindow, TResponseEnum>
    {
#region Constructors
        public FontDialogInfo()
        { }

        /// <summary>
        /// FontDialogInfo
        /// </summary>
        /// <param name="parent">TWindow</param>
        /// <param name="modal">bool</param>
        /// <param name="title">string</param>
        /// <param name="response">TResponseEnum</param>
        /// <param name="fontDescription">TFontDescriptor</param>
        public FontDialogInfo
        (
            TWindow parent,
            bool modal,
            string title,
            TResponseEnum response,
            TFontDescriptor fontDescription
        ) :
            base (parent, modal, title, response)
        {
            FontDescription = fontDescription;
        }

		#endregion Constructors
		#region Properties
		public TFontDescriptor FontDescription { get; set; }
		#endregion Properties

	}
}
