﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Ssepan.Utility.Core;

namespace Ssepan.Application.Core
{
    /// <summary>
    /// Manager for the run-time view model.
    /// </summary>
    /// <typeparam name="TIcon"></typeparam>
    /// <typeparam name="TViewModel"></typeparam>
    public static class ViewModelController<TIcon, TViewModel>
        where TViewModel :
            class,
            IViewModel<TIcon>,
            new()
        where TIcon : class
    {
		#region Properties
		/// <summary>
		/// Allow for named viewmodels of a given type.
		/// </summary>
		public static Dictionary<string, TViewModel> ViewModel { get; set; } = [];
		#endregion Properties

		#region Methods
		/// <summary>
		/// add new viewmodel to dictionary
		/// </summary>
		/// <param name="viewName"></param>
		/// <param name="viewModel"></param>
		/// <returns></returns>
		public static bool New
        (
            string viewName,
            TViewModel viewModel
        )
        {
            bool returnValue = default;
            try
            {
                //create new object
                ViewModel.Add(viewName, viewModel);

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        /// <summary>
        /// add new viewmodel to dictionary
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static bool New_
        (
            string viewName,
            TViewModel viewModel
        )
        {
            bool returnValue = default;
            try
            {
                returnValue = New(viewName, viewModel);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
        #endregion Methods
    }
}
