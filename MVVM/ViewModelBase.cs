﻿using System;
using System.ComponentModel;
using System.Reflection;
using Ssepan.Utility.Core;

namespace Ssepan.Application.Core
{
    /// <summary>
    /// ViewModelBase
    /// </summary>
    /// <typeparam name="TIcon">TIcon</typeparam>
    public abstract class ViewModelBase<TIcon> :
        INotifyPropertyChanged,
        IViewModel<TIcon>
        where TIcon : class
    {
        #region Declarations
        public const string ACTION_IN_PROGRESS = " ...";
        public const string ACTION_CANCELLED = " cancelled";
        public const string ACTION_DONE = " done";
        #endregion Declarations

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
        #endregion INotifyPropertyChanged

        #region Properties
        private string _StatusMessage;
        public string StatusMessage
        {
            get { return _StatusMessage; }
            set
            {
                if (value != _StatusMessage)
                {
                    _StatusMessage = value;
                    OnPropertyChanged(nameof(StatusMessage));
                }
            }
        }

        private string _ErrorMessage;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set
            {
                if (value != _ErrorMessage)
                {
                    _ErrorMessage = value;
                    OnPropertyChanged(nameof(ErrorMessage));
                }
            }
        }

        private string _CustomMessage;
        public string CustomMessage
        {
            get { return _CustomMessage; }
            set
            {
                if (value != _CustomMessage)
                {
                    _CustomMessage = value;
                    OnPropertyChanged(nameof(CustomMessage));
                }
            }
        }

        private string _ErrorMessageToolTipText;
        public string ErrorMessageToolTipText
        {
            get { return _ErrorMessageToolTipText; }
            set
            {
                if (value != _ErrorMessageToolTipText)
                {
                    _ErrorMessageToolTipText = value;
                    OnPropertyChanged(nameof(ErrorMessageToolTipText));
                }
            }
        }

        private int _ProgressBarValue;
        public int ProgressBarValue
        {
            get { return _ProgressBarValue; }
            set
            {
                if (value != _ProgressBarValue)
                {
                    _ProgressBarValue = value;
                    OnPropertyChanged(nameof(ProgressBarValue));
                }
            }
        }

        private int _ProgressBarMaximum;
        public int ProgressBarMaximum
        {
            get { return _ProgressBarMaximum; }
            set
            {
                if (value != _ProgressBarMaximum)
                {
                    _ProgressBarMaximum = value;
                    OnPropertyChanged(nameof(ProgressBarMaximum));
                }
            }
        }

        private int _ProgressBarMinimum;
        public int ProgressBarMinimum
        {
            get { return _ProgressBarMinimum; }
            set
            {
                if (value != _ProgressBarMinimum)
                {
                    _ProgressBarMinimum = value;
                    OnPropertyChanged(nameof(ProgressBarMinimum));
                }
            }
        }

        private int _ProgressBarStep;
        public int ProgressBarStep
        {
            get { return _ProgressBarStep; }
            set
            {
                if (value != _ProgressBarStep)
                {
                    _ProgressBarStep = value;
                    OnPropertyChanged(nameof(ProgressBarStep));
                }
            }
        }

        private bool _ProgressBarIsMarquee;
        public bool ProgressBarIsMarquee
        {
            get { return _ProgressBarIsMarquee; }
            set
            {
                if (value != _ProgressBarIsMarquee)
                {
                    _ProgressBarIsMarquee = value;
                    OnPropertyChanged(nameof(ProgressBarIsMarquee));
                }
            }
        }

        private bool _ProgressBarIsVisible;
        public bool ProgressBarIsVisible
        {
            get { return _ProgressBarIsVisible; }
            set
            {
                if (value != _ProgressBarIsVisible)
                {
                    _ProgressBarIsVisible = value;
                    OnPropertyChanged(nameof(ProgressBarIsVisible));
                }
            }
        }

        private bool _ActionIconIsVisible;
        public bool ActionIconIsVisible
        {
            get { return _ActionIconIsVisible; }
            set
            {
                if (value != _ActionIconIsVisible)
                {
                    _ActionIconIsVisible = value;
                    OnPropertyChanged(nameof(ActionIconIsVisible));
                }
            }
        }

        private TIcon _ActionIconImage;
        public TIcon ActionIconImage
        {
            get { return _ActionIconImage; }
            set
            {
                if (value != _ActionIconImage)
                {
                    _ActionIconImage = value;
                    OnPropertyChanged(nameof(ActionIconImage));
                }
            }
        }

        private bool _DirtyIconIsVisible;
        public bool DirtyIconIsVisible
        {
            get { return _DirtyIconIsVisible; }
            set
            {
                if (value != _DirtyIconIsVisible)
                {
                    _DirtyIconIsVisible = value;
                    OnPropertyChanged(nameof(DirtyIconIsVisible));
                }
            }
        }

        private TIcon _DirtyIconImage;
        public TIcon DirtyIconImage
        {
            get { return _DirtyIconImage; }
            set
            {
                if (value != _DirtyIconImage)
                {
                    _DirtyIconImage = value;
                    OnPropertyChanged(nameof(DirtyIconImage));
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Use when Marquee-style progress bar is not sufficient, and percentages must be indicated.
        /// WPF.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="errorMessage"></param>
        /// <param name="objImage">System.Windows.Controls.Image or System.Drawing.Image</param>
        /// <param name="isMarqueeProgressBarStyle"></param>
        /// <param name="progressBarValue"></param>
        /// <param name="doEventsWrapperDelegate"></param>
        public void StartProgressBar
        (
            string statusMessage,
            string errorMessage,
            TIcon objImage,
            bool isMarqueeProgressBarStyle,
            int progressBarValue,
            Action doEventsWrapperDelegate = null
        )
        {
            try
            {
                ProgressBarIsMarquee = isMarqueeProgressBarStyle;//set to blocks if actual percentage was used.
                ProgressBarValue = progressBarValue;//set to value if percentage used.
                //if Style is not Marquee, then we are marking either a count or percentage
                if (progressBarValue > ProgressBarMaximum)
                {
                    ProgressBarStep = 1;
                    ProgressBarValue = 1;
                }

                StatusMessage = statusMessage;
                ErrorMessage = errorMessage;
                //this.StatusBarErrorMessage.ToolTipText = errorMessage;

                ProgressBarIsVisible = true;

                ActionIconImage = objImage;
                //ActionIconWpfImage = objImage;
                if (objImage != null)
                {
                    ActionIconIsVisible = true;
                }

				//give the app time to draw the eye-candy, even if its only for an instant
				doEventsWrapperDelegate?.Invoke();
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Update percentage changes.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="progressBarValue"></param>
        /// <param name="doEventsWrapperDelegate"></param>
        public void UpdateProgressBar
        (
            string statusMessage,
            int progressBarValue,
            Action doEventsWrapperDelegate = null
        )
        {
            try
            {
                StatusMessage = statusMessage;
                //ErrorMessage = errorMessage;
                //this.StatusBarErrorMessage.ToolTipText = errorMessage;

                //if Style is not Marquee, then we are marking either a count or percentage
                //if we are simply counting, the progress bar will periodically need to adjust the Maximum.
                if (progressBarValue > ProgressBarMaximum)
                {
					ProgressBarMaximum *= 2;
                }
                ProgressBarValue = progressBarValue;

				//give the app time to draw the eye-candy, even if its only for an instant
				doEventsWrapperDelegate?.Invoke();
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Update message(s) only, without changing progress bar.
        /// Null parameter will leave a message unchanged;
        /// string.Empty will clear it.
        /// Optional doEvents flag will determine if
        /// messages are processed before continuing.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="errorMessage"></param>
        /// <param name="customMessage"></param>
        /// <param name="doEventsWrapperDelegate"></param>
        public void UpdateStatusBarMessages
        (
            string statusMessage,
            string errorMessage,
            string customMessage = null,
            Action doEventsWrapperDelegate = null
        )
        {
            try
            {
                if (statusMessage != null)
                {
                    StatusMessage = statusMessage;
                }
                if (errorMessage != null)
                {
                    ErrorMessage = errorMessage;
                    ErrorMessageToolTipText = errorMessage;
                }
                if (customMessage != null)
                {
                    CustomMessage = customMessage;
                }

				//give the app time to draw the eye-candy, even if its only for an instant
				doEventsWrapperDelegate?.Invoke();
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Stop progress bar and display messages.
        /// DoEvents will ensure messages are processed before continuing.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="errorMessage"></param>
        /// <param name="doEventsWrapperDelegate"></param>
        public void StopProgressBar
        (
            string statusMessage,
            string errorMessage = null,
            Action doEventsWrapperDelegate = null
        )
        {
            try
            {
                StatusMessage = statusMessage;
                //do not clear error at end of operation, clear it at start of operation
                if (errorMessage != null)
                {
                    ErrorMessage = errorMessage;
                    //this.StatusBarErrorMessage.ToolTipText = errorMessage;
                }

                ProgressBarIsMarquee = false;//reset back to marquee (default) in case actual percentage was used
                ProgressBarMaximum = 100;//ditto
                ProgressBarStep = 10;//ditto
                ProgressBarValue = 0;//ditto
                ProgressBarIsVisible = false;

                //implied <typeparam name="TIcon">string (Gtk.Image.IconName)</typeparam>
                ActionIconIsVisible = false;
                ActionIconImage = default;
				//ActionIconWpfImage = null;

				//give the app time to draw the eye-candy, even if its only for an instant
				doEventsWrapperDelegate?.Invoke();
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion Methods

    }
}
